package vse.ukol.domaci_ukol;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vse.ukol.domaci_ukol.*;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			FXMLLoader loader = new FXMLLoader();
			
			loader.setLocation(getClass().getResource("/Game.fxml"));
			
			Parent root = loader.load();
			
			GameController c = loader.getController();			
			 
			Scene scene = new Scene(root, 800, 650);
			
			scene.getStylesheets().add(getClass().getResource("/application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			
			primaryStage.show();
			
			Hra hra = new Hra();
			
			c.inicializuj(hra);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		//kontrola počtu parametrů
		if (args.length == 0) {
			//spuštění grafiky, parametr args je povinný, ale v tomto případě už ho v grafice nepoužijeme
			launch(args);
		} else {
			System.out.println("Neplatný parametr");		
		}
	}
}